const {Router} = require('express');
const UserService = require('../services/userService');
const {createUserValid, updateUserValid} = require('../middlewares/user.validation.middleware');
const {responseMiddleware} = require('../middlewares/response.middleware');

const router = Router();
// get all users
router.get('/', (req, res, next) => {
  try {
    res.data = UserService.getUsers();
  } catch (err) {
    res.err = err;
  } finally {
    next();
  }
}, responseMiddleware);
// get user
router.get('/:id', (req, res, next) => {
  try {
    res.data = UserService.getUser(req.params.id);
  } catch (err) {
    res.err = err;
  } finally {
    next();
  }
}, responseMiddleware);
// create user
router.post('/', createUserValid, (req, res, next) => {
  try {
    if (!res.err) {
      res.data = UserService.create(req.body);
    }
  } catch (err) {
    res.err = err;
  } finally {
    next();
  }
}, responseMiddleware);
// update user
router.put('/:id', updateUserValid, (req, res, next) => {
  try {
    if (!res.err) {
      res.data = UserService.update(req.params.id, req.body)
    }
  } catch (err) {
    res.err = err;
  } finally {
    next();
  }
}, responseMiddleware);
// delete user
router.delete('/:id', (req, res, next) => {
  try {
    res.data = UserService.delete(req.params.id);
  } catch (err) {
    res.err = err;
  } finally {
    next();
  }
}, responseMiddleware);

module.exports = router;
