const {UserRepository} = require('../repositories/userRepository');

class UserService {

  create(userData) {
    const user = UserRepository.create(userData);
    if (!user) {
      return null;
    }
    return user;
  }

  getUsers() {
    const users = UserRepository.getAll();
    if (!users) {
      return null;
    }
    return users;
  }

  getUser(userId) {
    const user = UserRepository.getOne({id: userId});
    if (!user) {
      throw Error('User not found');
    }
    return user;
  }

  delete(userId) {
    const user = UserRepository.delete(userId);
    if (!user.length) {
      throw Error('User not found');
    }
    return user;
  }

  update(userId, userData) {
    const user = UserRepository.update(userId, userData);
    if (!user.id) {
      throw Error('User not found');
    }
    return user;
  }

  search(search) {
    const item = UserRepository.getOne(search);
    if (!item) {
      return null;
    }
    return item;
  }
}

module.exports = new UserService();
