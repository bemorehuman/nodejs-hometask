const {Validator} = require("./validator");

class FighterValidator extends Validator {
  constructor(res) {
    super(res);
  }

  power(power = 0) {
    return (Number(power) >= 0 && Number(power) < 100) ? Number(power) : this.setError('Power must be number from 0 to 99');
  }

  defense(defense = 0) {
    return (Number(defense) >= 0 && Number(defense) <= 10) ? Number(defense) : this.setError('Defense must be number from 0 to 10');
  }

  health(health = 100) {
    return (Number(health) >= 0 && Number(health) <= 100) ? Number(health) : this.setError('Health must be number from 0 to 100');
  }

  name(name = '') {
    return name.trim().length >= 3 ? name.trim() : this.setError('Name should contain at least 3 symbols');
  }
}

exports.FighterValidator = FighterValidator;
