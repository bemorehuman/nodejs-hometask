const permitParams = (params, permittedFields) => {
  const obj = {...params};

  Object.keys(obj)
    .filter(key => !permittedFields.includes(key))
    .forEach(key => delete obj[key]);

  return obj;
}

exports.permitParams = permitParams;
