const {Validator} = require("./validator");

class UserValidator extends Validator {
  constructor(res) {
    super(res);
  }

  phoneNumber(phone) {
    return /^\+380\d{9}/.test(phone) ? phone : this.setError('Provide correct phone number: +380xxxxxxxxx');
  }

  firstName(firstName = '') {
    return firstName.trim() ? firstName : this.setError('First name is required');
  }

  lastName(lastName = '') {
    return lastName.trim() ? lastName : this.setError('Last name is required');
  }

  email(email) {
    return /@gmail\.com$/.test(email) ? email : this.setError('Allow only \'gmail\' emails');
  }

  password(password = '', minLength = 3) {
    return (password.trim().length >= minLength) ? password : this.setError('Password should contain at least 3 symbols');
  }
}

exports.UserValidator = UserValidator;
