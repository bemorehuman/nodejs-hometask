const {user} = require('../models/user');
const {permitParams} = require('../helpers/request-helper');
const {UserValidator} = require('../helpers/user-validator');

const requiredFields = ['firstName', 'lastName', 'email', 'password', 'phoneNumber'];

const createUserValid = (req, res, next) => {
  const validator = new UserValidator(res);
  let newUser = {...user};
  newUser = permitParams(newUser, requiredFields);

  newUser.firstName = validator.firstName(req.body.firstName);
  newUser.lastName = validator.lastName(req.body.lastName);
  newUser.email = validator.email(req.body.email);
  newUser.password = validator.password(req.body.password);
  newUser.phoneNumber = validator.phoneNumber(req.body.phoneNumber);

  req.body = newUser;

  next();
}

const updateUserValid = (req, res, next) => {
  const validator = new UserValidator(res);
  const newData = req.body;
  const updatedUser = {};

  for (let key in newData) {
    if (key in permitParams({...user}, requiredFields)) {
      const value = newData[key];
      updatedUser[key] = validator[key](value);

      if (!updatedUser[key]) {
        break;
      }
    }
  }

  req.body = updatedUser;

  next();
}

exports.createUserValid = createUserValid;
exports.updateUserValid = updateUserValid;
